package com.didahdx.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class CustomDialog extends DialogFragment {

    static final String TAG = CustomDialog.class.getSimpleName();
    EditText Input;
    TextView mActionOk, mActionCancel;

    public interface OnInputListener {
        void sendInput(String input);
    }

    public OnInputListener onInputListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_custom, container, false);
        Input = view.findViewById(R.id.body);
        mActionCancel = view.findViewById(R.id.cancel);
        mActionOk = view.findViewById(R.id.ok);

        mActionCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Closing Dialog", Toast.LENGTH_SHORT).show();
                getDialog().dismiss();
            }
        });


        mActionOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = Input.getText().toString().trim();

                if (!text.isEmpty() && !text.equals("")) {

//                    ((MainActivity) getActivity()).mInput_text.setText(text);
                    onInputListener.sendInput(text);
                    getDialog().dismiss();
                }

            }
        });

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            onInputListener = (OnInputListener) getActivity();

        } catch (ClassCastException e) {
            Log.e(TAG, "onAttach: " + e.getMessage());
        }
    }
}
