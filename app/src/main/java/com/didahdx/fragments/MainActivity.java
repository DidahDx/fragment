package com.didahdx.fragments;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, CustomDialog.OnInputListener{
    public static final String TAG = MainActivity.class.getSimpleName();

    Button mOpenDialog;
    TextView mInput_text;
    String input;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FindViewIds();
        init();
    }

    //initialise
    private void init() {
        mOpenDialog.setOnClickListener(this);
    }

    private void FindViewIds() {
        mOpenDialog = findViewById(R.id.button1);
        mInput_text = findViewById(R.id.text2);
    }


    @Override
    public void onClick(View v) {
        if (mOpenDialog==v){
            Toast.makeText(this, "openning dialog", Toast.LENGTH_SHORT).show();

            CustomDialog customDialog=new CustomDialog();
            customDialog.show(getSupportFragmentManager(),CustomDialog.TAG);
        }

    }

    @Override
    public void sendInput(String input) {
        mInput_text.setText(input);
    }
}
